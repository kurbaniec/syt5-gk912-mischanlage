## Kacper Urbaniec | 5AHIT | 20.11.2019  

#  "*Mischanlage*"

## Ziele

Du kannst Steuerprogramme mit komplexeren Bausteinen in FUP, ST und SFC erstellen und testen. Du kannst komplexere Visualisierungen inklusive Simulationslogik erstellen.

## Voraussetzungen

1. CodeSys 3.5 vorhanden (eigene Installation oder Citrix)

## Aufgaben

*Eine Mischanlage vermengt zwei verschiedene Flüssigkeiten. Dazu gibt es über einem Behälter zwei Zulaufventile, im Behälter einen Rührer und unter dem Behälter ein Ablaufventil. Der Behälter fasst 90l. Am Behälter gibt es zwei Sensoren, die anzeigen, ob der Behälter voll bzw. leer ist. Über Taster können die Zu- und Ablaufventile betätigt werden, wobei für das Ablaufventil zwei Taster vorzusehen sind (einer öffnet selbsthaltend, einer schließt, wobei die Schließfunktion dominant ausgeführt werden soll).*

*Um das Abfüllen der Mischung zu erleichtern, gibt es ein Signal Abfüllbereitschaft. Dieses signalisiert, dass ein Behälter unter dem Ablauf positioniert ist. Es darf nur abgefüllt werden, wenn Abfüllbereitschaft gegeben ist (Verriegelung!). Ist die Abfüllung erledigt, soll über das Signal "Abfüllen beendet" signalisiert werden, dass das Ventil geschlossen ist, und der Behälter entfernt werden kann.*

*Einzelheiten zu Sensoren und Aktoren können der Signaltabelle entnommen werden:*

| Eingänge                       | Symbol | Datentyp | Logische Zuordnung           | Bemerkung |
| ------------------------------ | ------ | -------- | ---------------------------- | --------- |
| Füllstandssensor Behälter voll | LSH-01 | BOOL     | Behälter voll -> LSH-01 = 0  |           |
| Füllstandssensor Behälter leer | LSL-02 | BOOL     | Behälter leer -> LSL-02 = 0  |           |
| Taster Flüssigkeit 1           | HS-03  | BOOL     | Taster gedrückt -> HS-03 = 1 |           |
| Taster Flüssigkeit 2           | HS-04  | BOOL     | Taster gedrückt -> HS-04 = 1 |           |
| Taster Ablauf öffnen           | HS-05  | BOOL     | Taster gedrückt -> HS-05 = 1 |           |
| Taster Ablauf schließen        | HS-06  | BOOL     | Taster gedrückt -> HS-06 = 1 |           |
| Abfüllbereitschaft             | XO-01  | BOOL     | bereit -> XO-01 = 1          |           |

| Ausgänge                   | Symbol | Datentyp | Logische Zuordnung            | Bemerkung                                                    |
| -------------------------- | ------ | -------- | ----------------------------- | ------------------------------------------------------------ |
| Zulaufventil Flüssigkeit 1 | YS-01  | BOOL     | YS-01 = 1 -> Ventil ist offen | Zulaufgeschwindigkeit 8 l/s                                  |
| Zulaufventil Flüssigkeit 2 | YS-02  | BOOL     | YS-02 = 1 -> Ventil ist offen | Zulaufgeschwindigkeit 6 l/s                                  |
| Ablaufventil               | YS-03  | BOOL     | YS-03 = 1 -> Ventil ist offen | Ablaufgeschwindigkeit 1 l/s                                  |
| Rührwerk (Rührer)          | NS-04  | BOOL     | NS-04 = 1 -> Rührer läuft     | Rechtslauf, 60 U/min                                         |
| Abfüllen beendet           | XO-02  | BOOL     | = 1 -> Abfüllen beendet       | bleibt solange 1 bis Abfüllbereitschaft von 0 auf 1 wechselt (steigende Flanke) |

*Für das Mischen gelten folgende Regeln:*

- *Es dürfen nie beide Flüssigkeiten gleichzeitig eingefüllt werden. Es darf auch nie gleichzeitig gefüllt und entleert werden. Dies muss durch **wechselseitig Verriegelung** ausgeschlossen werden.*
- *In einen leeren Behälter muss zuerst Flüssigkeit 1 eingefüllt werden, dabei darf der Rührer nicht in Betrieb sein.*
- *Wird Flüssigkeit (egal welche) zu einer bestehenden Füllung hinzugefügt, muss der Rührer dabei in Betrieb sein. Nach dem Schließen eines Zulaufventils, muss der Rührer noch 4 Sekunden nachlaufen. Während dieser Zeit, darf kein weiterer Zulauf erfolgen. Verwende dafür einen **Timer-Baustein**.*
- *Wird entleert, darf der Rührer nicht in Betrieb sein und es muss ein Behälter vorhanden sein.*

1. Erstelle die Steuerungslogik, die den gegebenen Sachverhalt abbildet. Achte dabei besonders auf die gegenseitigen Verriegelungen, damit die Anlage nicht in einen ungültigen Zustand gelangen kann. Versuche dabei gleiche Funktionalitäten in Funktionsbausteinen zusammenzufassen, um die Steuerungslogik möglichst übersichtlich zu gestalten.
2. Erstelle eine Visualisierung mit Hilfe eines Anlagenschemas. Darin sollen der Behälter, die Ventile etc. als solche erkennbar sein (siehe z.B. [dieses Beispiel](https://elearning.tgm.ac.at/pluginfile.php/134112/mod_assign/intro/Rührbehälter.png)). Taster können als Tasterelemente dargestellt werden. Der Füllstand des Behälters (Gesamt und Menge jeder Flüssigkeit) und das aktuelle Mischverhältnis sollen über Labels angezeigt werden. Damit dies funktioniert, muss Simulationslogik in einer eigenen POU erstellt werden (FUP, oder ST), die sich um die Berechnung der aktuellen Füllstände und das Auslösen der Füllstandssensoren kümmert.
3. Die Aktivität von Aktoren (Ventile, Rührwerk) soll durch farbliche Kennzeichnung dargestellt werden (z.B. offenes Ventil rot, geschlossenes schwarz - siehe Alarmzustand/-farbe in CodeSys). Die Abfüllbereitschaft soll über einen Schalter gesetzt werden können, das Abfüllende über eine Signallampe angezeigt werden.
4. (Erweitert) Wird der Flüssigkeitsstand im Behälter verändert, so soll dies **proportional richtig** angezeigt werden, d.h. der Flüssigkeitsstand "steigt" visuell beim Befüllen und fällt beim Entleeren. Die verschiedenen Flüssigkeiten sollen durch verschiedene Farben dargestellt werden, wobei diese optisch nicht vermischt werden, sondern stets getrennt dargestellt werden. Auf eine der Befüllungsstrategie folgenden Schichtung darf verzichtet werden. 
   Erweitere die Simulationslogik wenn erforderlich.
5. (Erweitert). Ergänze die Anlage um einen weiteren Taster. Dieser soll einen automatischen Ablauf der Mischanlage auslösen. Dies darf nur bei leerem Behälter und nicht schon laufendem Ablauf möglich sein.
   Der Ablauf soll zuerst eine Mischung aus 64l Flüssigkeit 1 und 24l Flüssigkeit 2 erzeugen. Danach soll das Rührwerk noch weitere 15 Sekunden die Mischung rühren. Ist die Abfüllung bereit, sollen von dem Gemisch 5 Behälter á 3 l  (verwende dazu einen Zähler-Baustein) und der Rest des Gemisches in einen 6. Behälter gefüllt werden.
   Verwende für diesen Ablauf ein **Sequentiell Function Chart** (Ablaufsprache).
   Erweitere die Simulationslogik, sodass die Abfüllbereitschaft gesetzt wird und das Abfüllende-Signal verarbeitet wird. Ein Behälterwechsel dauert 2s. Visualisiere dies auch ansprechend im Anlagenschema.

Die Anzeige soll auf 800x600px Platz finden. 

**Ergebnisse**

Das **Protokoll** muss folgende Elemente enthalten:

- alle erstellten, in eigenen Worten gut dokumentierten Quelltexte und Visualisierungen (Screenshots möglich)
- eine Erläuterung der Funktionsweise der Funktionsbausteine TON/TOF (und warum welcher gewählt wurde), RS/SR (und warum welcher gewählt wurde) und Flankenerkennung anhand der Verwendung im eigenen Programm. Sollte eine Bausteinart gar nicht verwendet worden sein (also z.B. gar kein Zähler) dann eine generische Erläuterung mit einem einfachen Beispiel hinzufügen.
- (Erweitert) Erläuterung der Funktionsweise der Funktionsbausteine CTD, CTU (und warum welcher gewählt wurde), sowie die Funktionsweise eines SFC (Schritte, Transitionen, Sprünge, Verzweigungen).
- ein Verzeichnis aller verwendeten Quellen (Links aus dem Internet etc.)

Die erstellte Lösung  muss in einem **Abgabegespräch** präsentiert werden. Termin bitte vereinbaren (möglichst in der nächsten möglichen Laboreinheit).

## Implementierung

Um zu Ermöglichen, dass die Variablen für die Ablauflogik auch vom Simulationsprogramm zugänglich sind (wichtig für Füllstandsensoren), habe ich diese in einer globalen Variablenliste definiert. 

Um für mich selbst die Programmierung einfacher zu gestalten und mit weniger Variablen zu hantieren, habe ich zwei Arrays erstellt, wobei eines die Eingänge und das andere die Ausgänge zusammenfasst.  Weiters wurde zur globalen Variablenliste eine Block Variable hinzugefügt, die wird später wichtig sein um den Rührer nach Beenden einer Flüssigkeitszugabe noch vier Sekunden lang weiter zu rotieren. 

Die Liste sieht wie folgt aus, oben ist ein Kommentar zu finden, der die Indexe der Arrays der Funktion zuordnet.

```pseudocode
(*
	input[0] - LSH-01 - Level Sensor tank full
	input[1] - LSL-02 - Level Sensor tank empty
	input[2] - HS-03 - Button liquid 1
	input[3] - HS-03 - Button liquid 2
	input[4] - HS-05 - Open drain
	input[5] - HS-06 - Close drain
	input[6] - X0-01 - Status if ready for fillig
	---
	output[0] - YS-01 - Intake liduid 1
	output[1] - YS-02 - Intake liquid 2
	output[2] - YS-03 - Drain öffnen
	output[3] - NS-04 - Agitator
	output[4] - X0-02 - Is filling done
*)

VAR_GLOBAL
	input: ARRAY [0..7] OF BOOL := [2(TRUE), 5(FALSE)];
	output: ARRAY [0..4] OF BOOL := [5(FALSE)];
	intake_block: BOOL;
END_VAR
```

Das Programm für die Umsetzung der Aufgabe heißt bei mir `Controller`. Im Programm selbst werden zwei Funktionen aufgerufen, `InputCheck` und `OutputCheck`, auf die später im Detail eingegangen wird. Grundsätzlich fragt das `Controller` Programm mittels `InputCheck` welcher Eingang gerade aufgerufen wird und führt die entsprechende Operation mittels `OutputCheck` durch.  Weiters wird geschaut, ob es sich um den "ersten" Durchgang handelt, dass bedeutet wenn ein Behälter leer ist, dann muss zuerst Flüssigkeit 1 eingefüllt werden, wobei dabei der Rührer nicht aktiviert sein darf. 

Im Programm wird auch die Verzögerung des Ausschaltens des Rührers  abgehandelt. Die Verzögerung wird mithilfe eines TON Bausteins realisiert, dessen Eingang auf `TRUE` gesetzt wird, wenn ein entsprechender Flag `GVL.intake_block`  auch diesen Wert annimmt. Mittels des `PT` Attributes kann die Verzögerung, in diesem Fall vier Sekunden, gesetzt werden. Wenn diese abgelaufen sind, wird das Attribut `Q` auf `TRUE` gesetzt, was angibt, dass der Timer abgelaufen ist. Dieser kann wieder neu gestartet werden, in dem der Block mittels `FALSE` auf dem Eingang resetet wird und später wieder auf `TRUE` gesetzt wird. Was mir Probleme bereitet hatte, war, dass ich nicht wusste, dass man den Timer Block jedes mal im Programm ausführen musste, nachdem der Eingang auf `TRUE` gesetzt wurde. Wird die Funktion daraufhin nie nochmalig ausgeführt, wird nie das `ET` Attribut aktualisiert, was für die verstrichene Zeit steht. Dadurch erreicht die verstrichene Zeit nie die Wartezeit und `Q` wird nie auf `TRUE` gesetzt.

```pseudocode
PROGRAM Controller
VAR
	current_input: INT;
	last_input: INT;
	first: BOOL := TRUE;
	timer: TON;
END_VAR
```

```pseudocode
// If tank is emptied, the whole process restarts
IF NOT GVL.input[1] THEN 
	first := TRUE;
	GVL.intake_block := FALSE;
	GVL.output[2] := FALSE;
END_IF

// Check which input is pressed
last_input := current_input;
current_input := InputCheck();

// Try to exececute task which is associated to the input
OutputCheck(first, current_input);

// If first task is finished (filling liquid 1) 
// agitator needs to be enabled when filling liquids
IF FIRST AND last_input = 2 AND current_input <> 2 THEN
	first := FALSE;
END_IF

// Call timer block when agitator should end rotation after timeout
IF GVL.intake_block THEN
	timer(IN:= TRUE, PT:= T#4S);
END_IF

// Stop agitator after four seconds after liquid filling is stopped
IF timer.Q THEN
	GVL.output[3] := FALSE;
	GVL.intake_block := FALSE;
	timer(IN:=FALSE);
END_IF
```

Die Funktion `InputCheck` macht nicht anderes als einen Wechselseitigen Ausschluss zwischen dem Füllen und Leeren des Tankes zu vollführen. Falls ein Eingang gedrückt wird, wird der assoziierte Index zur Weiterverarbeitung zurückgegeben. Falls mehrere Eingänge aktiviert sind, wird nur der "oberste" zurückgegeben, dabei wird beachtet, dass das Schließen des Auslaufventils dominant zum Öffnen des Ventils ist. Falls keine Eingabe getätigt wird, wird `-1` zurückgegeben.

```pseudocode
FUNCTION InputCheck : INT
VAR
	output: INT;
END_VAR
```

```pseudocode
output := -1;

// Perform mutual exclusion
IF GVL.input[2] THEN
	output := 2;
ELSIF GVL.input[3] THEN
	output := 3;
// Drain close is dominant to drain opening
ELSIF GVL.input[5] THEN
	output := 5;
ELSIF GVL.input[4] THEN
	output := 4;
END_IF

InputCheck := output;
```

In der Funktionen `OutputCheck` befindet sich die Logik zur Steuerung der Ausgänge. Als Parameter wird der Rückgabe Wert aus `InputCheck` verwendet, um zu sehen welche Operation/Aufgabe im Moment ausgeführt bzw. ausgeführt werden soll. Weiters wird der Parameter `first` übergeben, dieser besagt, ob es sich um den "ersten" Einfüllvorgang handelt. Dieser ist verbunden mit der Anforderung, dass beim ersten Füllen nur Flüssigkeit 1 rein gefüllt werden darf und, dass dabei nicht der Rührer an sein darf. 

Am Anfang des Programmes wird geschaut ob eine Flüssigkeit gefüllt werden soll und ob die Anforderungen dafür erfüllt sind. Danach wird, falls nicht eingefüllt wird, geschaut ob das Ablaufventil geöffnet werden soll. Danach wird geschaut, ob die Ausgänge für das Einfüllen der Flüssigkeiten geschlossen werden sollen, wenn der dazugehörige Eingang nicht aktiviert ist. Dabei fiel mir die ungewöhnliche Syntax für nicht gleich im Structured Text auf, anstatt `!=` wird nämlich `<>` verwendet. Bei der Operation ist es außerdem sehr wichtig, dass das Flag `GVL.intake_block` auf `TRUE` gesetzt wird. Dadurch wird im Hauptprogramm der Timer aktiviert, der verantwortlich ist, dass der Rührer sich noch vier Sekunden nach der Flüssigkeitszugabe dreht. Am Schluss des Programmes wird noch geschaut welchen Füllstand der Tank hat, falls er voll ist, werden alle Zuläufe geschlossen. Falls er leer ist, wird der Ablauf geschlossen. Außerdem wir der Status `Abfüllen beendet` entsprechend aktualisiert.

```pseudocode
FUNCTION OutputCheck
VAR_INPUT
	first: BOOL;
	input: INT;
END_VAR
VAR_OUTPUT
END_VAR
VAR
END_VAR
```

```pseudocode
// Fill tasks
IF (input = 2 OR input = 3) THEN
	// If tank is full no further liquid can be filled
	// Also, while the drain is open no liquid can be filled
	IF GVL.input[0] AND NOT GVL.output[2] THEN
		IF input = 2 AND NOT GVL.output[1] THEN
			IF NOT GVL.intake_block THEN
				GVL.output[0] := TRUE;
				// If the liquid is filled to an empty tank
				// do not start the agitator
				IF NOT first THEN
					GVL.output[3] := TRUE;
				END_IF
			END_IF
		ELSIF input = 3 AND NOT GVL.output[0] AND NOT first THEN
			IF NOT GVL.intake_block THEN
				GVL.output[1] := TRUE;
				GVL.output[3] := TRUE;
			END_IF
		END_IF
	END_IF
	
// Drain tasks
ELSIF input = 4 THEN
	// Do not open drain if agiator is moving or state is not ready
	IF NOT GVL.output[3] AND GVL.input[6] THEN
		GVL.output[2] := TRUE;
	END_IF
ELSIF input = 5 THEN
	GVL.output[2] := FALSE;
END_IF

// Close intakes of liquids if connected button is not pressed or tank is full
IF input <> 2 AND GVL.output[0] THEN
	IF first THEN
		first := FALSE;
	ELSE
		GVL.intake_block := TRUE;
	END_IF
	GVL.output[0] := FALSE;
END_IF
IF input <> 3 AND GVL.output[1] THEN
	GVL.intake_block := TRUE;
	GVL.output[1] := FALSE;
END_IF
IF NOT GVL.input[0] THEN
	GVL.intake_block := TRUE;
END_IF

// If tank is full always close intakes
IF NOT GVL.input[0] THEN
	GVL.output[0] := FALSE;
	GVL.output[1] := FALSE;
END_IF

// If tank is empty close drain
IF NOT GVL.input[1] THEN
	GVL.output[2] := FALSE;
END_IF

// Show filling status
IF GVL.input[6] THEN
	GVL.output[4] := FALSE;
ELSE
	GVL.output[4] := TRUE;
END_IF
```

Das letzte Programm was beschrieben wird, ist das Programm zur Simulation. Dieses ist separat zum Hauptprogramm und führt alle 200 Millisekunden einen Simulationsschritt durch. Die Realisierung mittels Timer sollte garantieren, dass trotz verschiedener Zykluszeiten die Logik gleich ablaufen sollte. 

Falls keine Abfüllbereitschaft herrscht wird der Füllstands des Behälters, in dem abgefüllt wird, auf 0 gesetzt. Dadurch kann das Abfüllen resetet werden. Bei der Simulation des Tankes gibt es drei Werte zu beachten: `liquid1`, `liquid2` und `mix`. `liquid1` beschreibt den Pegel der Flüssigkeit 1 im Tank, `liquid2` ist analog für Flüssigkeit 2. `mix` dagegen beschreibt die gesamte Füllmenge des Tankes, die aus den beiden Flüssigkeiten besteht. 

Beim Einfüllen wird einfach der derzeitige Wert plus 100 gerechnet, beim Abfüllen aber werden die einzelnen Flüssigkeiten korrekt nach ihrem Anteil an der Gesamtmenge reduziert.  Wenn im Tank also Flüssigkeit 1 doppelt so viel vorhanden ist wie Flüssigkeit 2 , wird 66 bzw. 33 von 100 den Flüssigkeiten abgerechnet. Bei der Berechnung ist es unbedingt wichtig, das die Werte in eine Gleitkommazahl umgewandelt werden, da sonst 0 zurückgegeben wird. Dazu wird die Methode `INT_TO_REAL` verwendet. Zuerst suchte ich nach einer Funktion für `Float`  oder `Double`, doch im Structured Text werden Gleitkommazahlen mit dem Typ `REAL` repräsentiert.  

Die Simulation schaltet weiters entsprechen die Sensoren für die Füllzustände und ist außerdem für die Rotierung des Rührers in der Visualisierung verantwortlich, in dem für das Bild des Rührers ein Winkel von der Simulation gesetzt, der ständig aktualisiert wird und dadurch das Gefühl einer Drehung vermittelt.

```pseudocode
PROGRAM Simulation
VAR
	liquid_1: INT := 0;
	liquid_2: INT := 0;
	mix: INT := 0;
	tank_max: INT := 9000;
	drain_1: INT;
	drain_2: INT;
	container: INT;
	timer: TON;
	agitator_spin: INT := 0;
END_VAR
```

```pseudocode
// Every 200 ms the simulation is triggered
timer(IN:=TRUE, PT:=T#200MS);
IF timer.Q THEN
	// If not ready for filling reset fill simulation
	IF NOT GVL.input[6] THEN
		container := 0;
	END_IF
	// If intake 1 is open fill liquid 1
	IF GVL.output[0] THEN
		liquid_1 := liquid_1 + 100;
		mix := mix + 100;
	END_IF
	// If intake 2 is open fill liquid 2
	IF GVL.output[1] THEN
		liquid_2 := liquid_2 + 100;
		mix := mix + 100;
	END_IF
	// If drain is open and the tank is not empty drain the liquid mix
	// to the bottom container
	IF GVL.output[2] AND GVL.input[1] THEN
		drain_1:= REAL_TO_INT(INT_TO_REAL(liquid_1) / INT_TO_REAL(liquid_1 + liquid_2) * 100);
		liquid_1 := liquid_1 - drain_1;
		drain_2:= REAL_TO_INT(INT_TO_REAL(liquid_2) / INT_TO_REAL(liquid_1 + liquid_2) * 100);
		liquid_2 := liquid_2 - drain_2;
		mix := mix - drain_1 - drain_2;
		container := container + drain_1 + drain_2;
	END_IF
	// If liquid mix is over the maximum tank capacity set full sensor
	IF MIX >= tank_max THEN
		GVL.input[0] := FALSE;
	// If tank is empty set empty sensor
	ELSIF MIX <= 0 THEN
		GVL.input[1] := FALSE;
	// If liquid mix is between minimum and maximum set both sensors
	ELSE
		GVL.input[0] := TRUE;
		GVL.input[1] := TRUE;
	END_IF
	// If agitator is moving, spin it in the simulation
	IF GVL.output[3] THEN
		agitator_spin := agitator_spin + 20;
		IF agitator_spin > 360 THEN
			agitator_spin := 0;
		END_IF
	END_IF
	// Prepare for next simulation run
	timer(IN:=FALSE);
END_IF
```

### Visualisierung

Nachfolgend werden mehrere Zustände des Programmes mittels der Visualisierung dargestellt:

*Anfangszustand, alle Ventile zu, Abfüllbereitschaft nicht gegeben.*

![](images/1.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Flüssigkeit 1 wurde für einen Moment eingefüllt und wieder geschlossen.*

![](images/2.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Flüssigkeit 2 wurde für einen Moment eingefüllt und wieder geschlossen. Da vier Sekunden noch nicht abgelaufen sind läuft der Rührer noch.*

![](images/3.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Nachdem der Rührer fertig war, wurde Flüssigkeit 1 bis zum Maximum des Tankes gefüllt. Da vier Sekunden abgelaufen sind, läuft der Rührer nicht mehr.*

![](images/4.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Abfüllbereitschaft wird eingeleitet.*

![](images/5.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Flüssigkeit wird zu einem gewissen Grad entleert, dann wird der Ablauf gestoppt.*

![](images/6.PNG)

<div style="page-break-after: always; visibility: hidden"> 
\pagebreak 
</div>

*Restliche Flüssigkeit wird komplett abgefüllt.*

![](images/7.PNG)



## Quellen

* [Codesys Array]( https://forum.codesys.com/viewtopic.php?t=6627 )
* [Codesys Timer]( https://stackoverflow.com/questions/25682962/timers-in-plc-structured-text )
* [Codesys For-Loop]( https://forum.codesys.com/viewtopic.php?t=4935 )
* [Codesys Pass-By-Reference]( https://help.codesys.com/api-content/2/codesys/3.5.13.0/en/_cds_vartypes_var_in_out/ )
* [Codesys Bilder einbetten]( https://www.youtube.com/watch?v=ZhIKE7JNFZE )
* [Codesys Bilder rotieren]( https://www.youtube.com/watch?v=pPHxgka5Pq8 )
* [Codesys Int - Real - Umwandlung]( https://forum.codesys.com/viewtopic.php?t=7545 )
* [Rührer Bild]( http://image.ourclipart.com/1799/17990203/hydrofoil-impeller.jpg  )

